package eu.hinterdorfer.decisiontool

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import eu.hinterdorfer.decisiontool.ui.components.create_decision_dialog.CreateDecisionDialog
import eu.hinterdorfer.decisiontool.ui.components.create_decision_dialog.CreateDecisionDialogActions
import eu.hinterdorfer.decisiontool.ui.components.sidenav.Sidenav
import eu.hinterdorfer.decisiontool.ui.components.sidenav.SidenavActions
import eu.hinterdorfer.decisiontool.ui.navigation.HomeRoute
import eu.hinterdorfer.decisiontool.ui.navigation.MainNavHost
import eu.hinterdorfer.decisiontool.ui.theme.DecisionToolTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DecisionToolTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val scope = rememberCoroutineScope()
                    val openNewDecisionDialog = remember { mutableStateOf(false) }
                    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
                    val navController = rememberNavController()
                    MainNavigationDrawer(
                        coroutineScope = scope,
                        drawerState = drawerState,
                        openNewDecisionDialog = openNewDecisionDialog,
                        navController = navController
                    )
                    {
                        MainContent(
                            navHostController = navController,
                            drawerState = drawerState,
                            coroutineScope = scope,
                            openNewDecisionDialog = openNewDecisionDialog,
                        )
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainContent(
    navHostController: NavHostController,
    drawerState: DrawerState,
    coroutineScope: CoroutineScope,
    openNewDecisionDialog: MutableState<Boolean>,
    mainViewModel: MainViewModel = hiltViewModel(),
) {
    when {
        openNewDecisionDialog.value ->
            CreateDecisionDialog(
                actions = CreateDecisionDialogActions(
                    dismissDialog = {
                        openNewDecisionDialog.value = false
                    },
                    confirmDialog = { name: String, description: String ->
                        coroutineScope.launch { mainViewModel.saveDecision(name, description) }
                        openNewDecisionDialog.value = false
                    })
            )
    }
    Scaffold(
        topBar = {
            val uiState by mainViewModel.state.collectAsState()
            TopAppBar(
                title = { Text(text = uiState.title) },
                navigationIcon = {
                    IconButton(onClick = { coroutineScope.launch { drawerState.open() } }) {
                        Icon(
                            imageVector = Icons.Rounded.Menu,
                            contentDescription = "Open Menu"
                        )
                    }
                })
        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                coroutineScope.launch {
                    openNewDecisionDialog.value = true
                }
            }) {
                Icon(
                    imageVector = Icons.Rounded.Add,
                    contentDescription = "Add new decision"
                )
            }
        }
    ) { innerPaddingValues ->
        MainNavHost(paddingValues = innerPaddingValues, navController = navHostController)
    }
}


@Composable
fun MainNavigationDrawer(
    coroutineScope: CoroutineScope,
    navController: NavHostController,
    drawerState: DrawerState,
    openNewDecisionDialog: MutableState<Boolean>,
    content: @Composable () -> Unit,
) {
    Sidenav(actions = SidenavActions(
        navigateToHome = {
            coroutineScope.launch {
                navController.navigate(HomeRoute.route)
                drawerState.close()
            }
        },
        navigateToNewDecision = {
            coroutineScope.launch {
                drawerState.close()
                openNewDecisionDialog.value = true
            }
        },
        navigateToDecision = { decisionId ->
            coroutineScope.launch {
                navController.navigate("decision/$decisionId")
                drawerState.close()
            }
        }
    ), drawerState = drawerState) {
        content()
    }

}


