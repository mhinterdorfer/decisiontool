package eu.hinterdorfer.decisiontool

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DecisionTool : Application()