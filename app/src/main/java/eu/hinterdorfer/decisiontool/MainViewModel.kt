package eu.hinterdorfer.decisiontool

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.hinterdorfer.decisiontool.data.model.Decision
import eu.hinterdorfer.decisiontool.data.repository.DecisionRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val decisionRepository: DecisionRepository
) : ViewModel() {

    private val _state = MutableStateFlow(MainState())

    val state: StateFlow<MainState> = _state.asStateFlow()

    suspend fun saveDecision(name: String, description: String) {
        decisionRepository.insertAllDecisions(Decision(title = name, description = description))
    }

    fun changeTitle(title: String) {
        viewModelScope.launch {
            _state.value = _state.value.copy(
                title = title
            )
        }
    }
}

data class MainState(val title: String = "Decision Tool")