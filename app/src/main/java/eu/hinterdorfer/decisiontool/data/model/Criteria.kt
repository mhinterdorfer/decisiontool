package eu.hinterdorfer.decisiontool.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = Decision::class,
        parentColumns = ["id"],
        childColumns = ["decisionId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class Criteria(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    val text: String,
    val decisionId: Long,
    var weight: Float = 0.0f
)
