package eu.hinterdorfer.decisiontool.data.dao

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Embedded
import androidx.room.Insert
import androidx.room.Query
import eu.hinterdorfer.decisiontool.data.model.Contestant
import eu.hinterdorfer.decisiontool.data.model.Rating
import kotlinx.coroutines.flow.Flow


@Dao
interface RatingDAO {

    @Query("DELETE FROM rating WHERE contestantId = :contestantId")
    suspend fun deleteByContestant(contestantId: Long)

    @Insert
    fun insertRatings(vararg rating: Rating)

    @Query(
        """SELECT contestant.*, sum(rating.value * criteria.weight) as result 
            from rating 
            join criteria on rating.criteriaId = criteria.id 
            join contestant on rating.contestantId = contestant.id 
            where rating.decisionId=:decisionId 
            group by rating.contestantId """
    )
    fun queryResults(decisionId: Long): Flow<List<ContestantAndResult>>

}

data class ContestantAndResult(
    @Embedded val contestant: Contestant,
    @ColumnInfo(name = "result") val result: Float
)