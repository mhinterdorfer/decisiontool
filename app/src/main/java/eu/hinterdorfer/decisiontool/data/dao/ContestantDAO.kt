package eu.hinterdorfer.decisiontool.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import eu.hinterdorfer.decisiontool.data.model.Contestant
import kotlinx.coroutines.flow.Flow


@Dao
interface ContestantDAO {
    @Query("SELECT * FROM contestant")
    fun findAll(): Flow<List<Contestant>>

    @Insert
    suspend fun insertAll(vararg contestant: Contestant)

    @Delete
    suspend fun delete(vararg contestant: Contestant)

    @Query("SELECT * FROM contestant WHERE id=:id")
    fun findById(id: Long): Flow<Contestant>

}