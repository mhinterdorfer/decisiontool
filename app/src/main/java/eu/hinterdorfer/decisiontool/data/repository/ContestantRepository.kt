package eu.hinterdorfer.decisiontool.data.repository

import eu.hinterdorfer.decisiontool.data.dao.ContestantDAO
import eu.hinterdorfer.decisiontool.data.model.Contestant
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface IContestantRepository {
    suspend fun findAllContestants(): Flow<List<Contestant>>

    suspend fun insertAllContestants(vararg contestant: Contestant)
    suspend fun deleteContestant(contestant: Contestant)

    suspend fun findById(id: Long): Flow<Contestant>
}

class ContestantRepository @Inject constructor(private val contestantDAO: ContestantDAO) :
    IContestantRepository {
    override suspend fun findAllContestants(): Flow<List<Contestant>> {
        return withContext(IO) {
            contestantDAO.findAll()
        }
    }

    override suspend fun insertAllContestants(vararg contestant: Contestant) {
        withContext(IO) {
            contestantDAO.insertAll(*contestant)
        }
    }

    override suspend fun deleteContestant(contestant: Contestant) {
        withContext(IO) {
            contestantDAO.delete(contestant)
        }
    }

    override suspend fun findById(id: Long): Flow<Contestant> {
        return withContext(IO) {
            contestantDAO.findById(id)
        }
    }
}