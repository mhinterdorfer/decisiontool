package eu.hinterdorfer.decisiontool.data.repository

import eu.hinterdorfer.decisiontool.data.dao.CriteriaDAO
import eu.hinterdorfer.decisiontool.data.model.Criteria
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface ICriteriaRepository {
    suspend fun findAllCriterias(): Flow<List<Criteria>>

    suspend fun insertAllCriterias(vararg criteria: Criteria)
    suspend fun deleteCriteria(vararg criteria: Criteria)
    suspend fun updateWeight(criteriaId: Long, newWeight: Float)
}

class CriteriaRepository @Inject constructor(private val criteriaDAO: CriteriaDAO) :
    ICriteriaRepository {
    override suspend fun findAllCriterias(): Flow<List<Criteria>> {
        return withContext(IO) {
            criteriaDAO.findAll()
        }
    }

    override suspend fun insertAllCriterias(vararg criteria: Criteria) {
        withContext(IO) {
            criteriaDAO.insertAll(*criteria)
        }
    }

    override suspend fun deleteCriteria(vararg criteria: Criteria) {
        withContext(IO) {
            criteriaDAO.deleteAll(*criteria)
        }
    }

    override suspend fun updateWeight(criteriaId: Long, newWeight: Float) {
        withContext(IO) {
            criteriaDAO.updateWeight(criteriaId, newWeight)
        }
    }
}