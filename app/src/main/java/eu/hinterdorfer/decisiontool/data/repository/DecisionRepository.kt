package eu.hinterdorfer.decisiontool.data.repository

import eu.hinterdorfer.decisiontool.data.dao.DecisionDAO
import eu.hinterdorfer.decisiontool.data.model.Contestant
import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.model.Decision
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface IDecisionRepository {
    suspend fun findAllDecisions(): Flow<List<Decision>>

    suspend fun insertAllDecisions(vararg decision: Decision)

    suspend fun countOpenDecisions(): Flow<Int>

    suspend fun findById(id: Long): Flow<Decision>

    suspend fun findContestantsById(id: Long): Flow<List<Contestant>>

    suspend fun findCriteriasById(decisionId: Long): Flow<List<Criteria>>

    suspend fun deleteDecision(decision: Decision)
}

class DecisionRepository @Inject constructor(private val decisionDAO: DecisionDAO) :
    IDecisionRepository {
    override suspend fun findAllDecisions(): Flow<List<Decision>> {
        return withContext(IO) {
            decisionDAO.findAll()
        }
    }

    override suspend fun insertAllDecisions(vararg decision: Decision) {
        withContext(IO) {
            decisionDAO.insertAll(*decision)
        }
    }

    override suspend fun countOpenDecisions(): Flow<Int> {
        return withContext(IO) {
            decisionDAO.countOpenDecisions()
        }
    }

    override suspend fun findById(id: Long): Flow<Decision> {
        return withContext(IO) {
            decisionDAO.findById(id)
        }
    }

    override suspend fun findContestantsById(id: Long): Flow<List<Contestant>> {
        return withContext(IO) {
            decisionDAO.findContestantsById(id)
        }
    }

    override suspend fun findCriteriasById(decisionId: Long): Flow<List<Criteria>> {
        return withContext(IO) {
            decisionDAO.findCriteriasById(decisionId)
        }

    }

    override suspend fun deleteDecision(decision: Decision) {
        withContext(IO) {
            decisionDAO.deleteDecision(decision)
        }
    }
}