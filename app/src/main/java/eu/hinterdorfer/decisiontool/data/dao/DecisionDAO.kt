package eu.hinterdorfer.decisiontool.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import eu.hinterdorfer.decisiontool.data.model.Contestant
import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.model.Decision
import kotlinx.coroutines.flow.Flow


@Dao
interface DecisionDAO {
    @Query("SELECT * FROM decision")
    fun findAll(): Flow<List<Decision>>

    @Insert
    suspend fun insertAll(vararg decisions: Decision)

    @Query("SELECT count(id) from decision")
    fun countOpenDecisions(): Flow<Int>

    @Query("SELECT * FROM decision WHERE id=:id")
    fun findById(id: Long): Flow<Decision>

    @Query("SELECT * FROM contestant WHERE decisionId=:id")
    fun findContestantsById(id: Long): Flow<List<Contestant>>

    @Query("SELECT * FROM criteria WHERE decisionId=:id")
    fun findCriteriasById(id: Long): Flow<List<Criteria>>

    @Delete
    fun deleteDecision(decision: Decision)
}