package eu.hinterdorfer.decisiontool.data

import androidx.room.Database
import androidx.room.RoomDatabase
import eu.hinterdorfer.decisiontool.data.dao.ContestantDAO
import eu.hinterdorfer.decisiontool.data.dao.CriteriaDAO
import eu.hinterdorfer.decisiontool.data.dao.DecisionDAO
import eu.hinterdorfer.decisiontool.data.dao.RatingDAO
import eu.hinterdorfer.decisiontool.data.model.Contestant
import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.model.Decision
import eu.hinterdorfer.decisiontool.data.model.Rating

@Database(
    entities = [Contestant::class, Decision::class, Criteria::class, Rating::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    // register DAOs here
    abstract fun decisionDAO(): DecisionDAO
    abstract fun contestantDAO(): ContestantDAO
    abstract fun criteriaDAO(): CriteriaDAO
    abstract fun ratingDAO(): RatingDAO
}