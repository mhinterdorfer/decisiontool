package eu.hinterdorfer.decisiontool.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import eu.hinterdorfer.decisiontool.data.model.Criteria
import kotlinx.coroutines.flow.Flow


@Dao
interface CriteriaDAO {
    @Query("SELECT * FROM criteria")
    fun findAll(): Flow<List<Criteria>>

    @Insert
    suspend fun insertAll(vararg criteria: Criteria)

    @Delete
    suspend fun deleteAll(vararg criteria: Criteria)

    @Query("UPDATE criteria SET weight = :newWeight WHERE id = :criteriaId")
    suspend fun updateWeight(criteriaId: Long, newWeight: Float)

}