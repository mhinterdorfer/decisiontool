package eu.hinterdorfer.decisiontool.data.model

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    primaryKeys = ["decisionId", "criteriaId", "contestantId"],
    foreignKeys = [
        ForeignKey(
            entity = Criteria::class,
            parentColumns = ["id"],
            childColumns = ["criteriaId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Decision::class,
            parentColumns = ["id"],
            childColumns = ["decisionId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Contestant::class,
            parentColumns = ["id"],
            childColumns = ["contestantId"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Rating(
    val decisionId: Long,
    val criteriaId: Long,
    val contestantId: Long,
    var value: Float
)
