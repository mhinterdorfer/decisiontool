package eu.hinterdorfer.decisiontool.data.repository

import eu.hinterdorfer.decisiontool.data.dao.ContestantAndResult
import eu.hinterdorfer.decisiontool.data.dao.RatingDAO
import eu.hinterdorfer.decisiontool.data.model.Rating
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface IRatingRepository {
    suspend fun deleteByContestant(contestantId: Long)
    suspend fun saveRating(rating: Rating)

    suspend fun queryResults(decisionId: Long): Flow<List<ContestantAndResult>>
}

class RatingRepository @Inject constructor(private val ratingDAO: RatingDAO) :
    IRatingRepository {
    override suspend fun deleteByContestant(contestantId: Long) {
        withContext(IO) {
            ratingDAO.deleteByContestant(contestantId)
        }
    }

    override suspend fun saveRating(rating: Rating) {
        withContext(IO) {
            ratingDAO.insertRatings(rating)
        }
    }

    override suspend fun queryResults(decisionId: Long): Flow<List<ContestantAndResult>> {
        return withContext(IO) {
            ratingDAO.queryResults(decisionId)
        }
    }
}