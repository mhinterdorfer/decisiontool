package eu.hinterdorfer.decisiontool.ui.views.results

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import co.yml.charts.axis.AxisData
import co.yml.charts.common.model.Point
import co.yml.charts.ui.barchart.BarChart
import co.yml.charts.ui.barchart.models.BarChartData
import co.yml.charts.ui.barchart.models.BarData
import co.yml.charts.ui.barchart.models.BarStyle
import eu.hinterdorfer.decisiontool.ui.components.labeled_content.LabeledContent

@Composable
fun ResultsScreen(
    viewModel: ResultsViewModel = hiltViewModel(),
    actions: ResultsActions,
) {
    val coroutineScope = rememberCoroutineScope()
    val uiState by viewModel.state.collectAsState()

    Column(
        modifier = Modifier
            .padding(6.dp)
            .fillMaxWidth(),
    ) {
        ElevatedCard(
            modifier = Modifier.fillMaxWidth()
        ) {
            LabeledContent(label = "Decision") {
                Text(
                    text = uiState.decision?.title ?: "?",
                    style = MaterialTheme.typography.headlineLarge
                )
            }
        }
        if (uiState.results.isNotEmpty()) {
            Chart()
        }
    }
}

@Composable
fun Chart(viewModel: ResultsViewModel = hiltViewModel()) {
    val uiState by viewModel.state.collectAsState()
    val backgroundColor = MaterialTheme.colorScheme.onBackground
    val barsData = uiState.results.mapIndexed { index, result ->
        BarData(point = Point(index.toFloat() + 1f, result.result), label = result.contestant.name)
    }

    val xAxisData = AxisData.Builder()
        .bottomPadding(100.dp)
        .axisLabelAngle(45f)
        .labelData { index -> barsData[index].label }
        .backgroundColor(backgroundColor)
        .steps(barsData.size)
        .startDrawPadding(25.dp)
        .shouldDrawAxisLineTillEnd(true)
        .build()
    val yAxisData = AxisData.Builder()
        .steps(10)
        .labelAndAxisLinePadding(20.dp)
        .axisOffset(20.dp)
        .labelData { index -> index.toString() }
        .backgroundColor(backgroundColor).build()

    val barChartData = BarChartData(
        chartData = barsData,
        xAxisData = xAxisData,
        yAxisData = yAxisData,
        backgroundColor = backgroundColor,
        barStyle = BarStyle(paddingBetweenBars = 50.dp)
    )

    BarChart(
        modifier = Modifier
            .height(350.dp)
            .fillMaxWidth(), barChartData = barChartData
    )

}

