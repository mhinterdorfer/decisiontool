package eu.hinterdorfer.decisiontool.ui.components.create_contestant_dialog


/**
 * Home Actions emitted from the UI Layer
 * passed to the coordinator to handle
 **/
data class CreateContestantDialogActions(
    val dismissDialog: () -> Unit = {},
    val confirmDialog: (name: String, information: String) -> Unit = { name: String, information: String ->
        println(
            "$name, $information"
        )
    }
)