package eu.hinterdorfer.decisiontool.ui.views.results

import eu.hinterdorfer.decisiontool.data.dao.ContestantAndResult
import eu.hinterdorfer.decisiontool.data.model.Decision

data class ResultsState(
    val decision: Decision? = null,
    val results: List<ContestantAndResult> = emptyList()
)

data class ResultsActions(
    val navigateBack: () -> Unit = {},
)
