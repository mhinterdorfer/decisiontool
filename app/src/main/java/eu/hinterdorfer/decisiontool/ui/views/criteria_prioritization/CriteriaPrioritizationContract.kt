package eu.hinterdorfer.decisiontool.ui.views.criteria_prioritization

import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.model.Decision

data class CriteriaPrioritizationState(
    val decision: Decision? = null,
    val criteria: Map<Criteria, Float> = emptyMap(),
    val combinations: List<Pair<Criteria, Criteria>> = emptyList()
)

data class CriteriaPrioritizationActions(
    val cancel: () -> Unit = {},
    val afterSave: () -> Unit = {}
)
