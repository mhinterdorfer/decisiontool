package eu.hinterdorfer.decisiontool.ui.views.criteria_prioritization

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.repository.CriteriaRepository
import eu.hinterdorfer.decisiontool.data.repository.DecisionRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CriteriaPrioritizationViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val decisionRepository: DecisionRepository,
    private val criteriaRepository: CriteriaRepository,
) : ViewModel() {

    private val _state = MutableStateFlow(CriteriaPrioritizationState())

    val state: StateFlow<CriteriaPrioritizationState> = _state.asStateFlow()

    private val decisionId: Long = checkNotNull(savedStateHandle["decisionId"])

    init {
        viewModelScope.launch {
            decisionRepository.findById(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        decision = result
                    )
                }
            }.launchIn(viewModelScope)
            decisionRepository.findCriteriasById(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        criteria = result.associateWith { 0.0f }.toMutableMap(),
                        combinations =
                        result.flatMapIndexed { index: Int, criteriaA: Criteria ->
                            result.drop(index)
                                .filter { criteriaB: Criteria -> criteriaA != criteriaB }
                                .map { criteriaB: Criteria -> Pair(criteriaA, criteriaB) }
                        }
                    )
                }
            }.launchIn(viewModelScope)
        }
    }

    fun prioritizeCriteria(criteria: Criteria) {
        _state.update {
            _state.value.copy(
                criteria = it.criteria.plus(criteria to (it.criteria[criteria]?.plus(1.0f) ?: 0.0f))
            )

        }
    }

    suspend fun saveWeights() {
        _state.value.criteria.forEach { entry ->
            criteriaRepository.updateWeight(
                entry.key.id,
                entry.value / _state.value.combinations.size
            )
        }
    }
}