package eu.hinterdorfer.decisiontool.ui.components.create_contestant_dialog


/**
 * Home Actions emitted from the UI Layer
 * passed to the coordinator to handle
 **/
data class ConfirmationDialogActions(
    val dismissDialog: () -> Unit = {},
    val confirmDialog: () -> Unit = {}
)