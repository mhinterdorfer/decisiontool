package eu.hinterdorfer.decisiontool.ui.theme

import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun ColumnScope.SpacerSmall() =
    Spacer(modifier = Modifier.height(4.dp))

@Composable
fun ColumnScope.SpacerMedium() =
    Spacer(modifier = Modifier.height(8.dp))

@Composable
fun ColumnScope.SpacerLarge() =
    Spacer(modifier = Modifier.height(16.dp))

@Composable
fun RowScope.SpacerSmall() =
    Spacer(modifier = Modifier.width(4.dp))

@Composable
fun RowScope.SpacerMedium() =
    Spacer(modifier = Modifier.width(8.dp))

@Composable
fun RowScope.SpacerLarge() =
    Spacer(modifier = Modifier.width(16.dp))
