package eu.hinterdorfer.decisiontool.ui.views.decision_overview

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.ListItemDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import eu.hinterdorfer.decisiontool.R
import eu.hinterdorfer.decisiontool.ui.components.confirmation_dialog.ConfirmationDialog
import eu.hinterdorfer.decisiontool.ui.components.create_contestant_dialog.ConfirmationDialogActions
import eu.hinterdorfer.decisiontool.ui.components.create_contestant_dialog.CreateContestantDialog
import eu.hinterdorfer.decisiontool.ui.components.create_contestant_dialog.CreateContestantDialogActions
import eu.hinterdorfer.decisiontool.ui.components.create_criteria_dialog.CreateCriteriaDialog
import eu.hinterdorfer.decisiontool.ui.components.create_criteria_dialog.CreateCriteriaDialogActions
import eu.hinterdorfer.decisiontool.ui.components.dismissable_list.DismissibleList
import eu.hinterdorfer.decisiontool.ui.components.dismissable_list.DismissibleListActions
import eu.hinterdorfer.decisiontool.ui.theme.SpacerLarge
import eu.hinterdorfer.decisiontool.ui.theme.SpacerSmall
import kotlinx.coroutines.launch

@Composable
fun DecisionOverviewScreen(
    viewModel: DecisionOverviewViewModel = hiltViewModel(),
    actions: DecisionOverviewActions,
) {
    val uiState by viewModel.state.collectAsState()
    val openCreateContestantDialog = remember {
        mutableStateOf(false)
    }
    val openCreateCriteriaDialog = remember {
        mutableStateOf(false)
    }
    val openConfirmDeletionDialog = remember {
        mutableStateOf(false)
    }
    val coroutineScope = rememberCoroutineScope()
    uiState.decision?.let { decision ->
        when {
            openCreateContestantDialog.value -> CreateContestantDialog(
                actions = CreateContestantDialogActions(
                    dismissDialog = {
                        openCreateContestantDialog.value = false
                    },
                    confirmDialog = { name, information ->
                        coroutineScope.launch {
                            viewModel.saveContestant(
                                uiState.decision!!.id, name, information
                            )
                        }
                        openCreateContestantDialog.value = false
                    }), decision = decision
            )
        }
    }
    uiState.decision?.let { decision ->
        when {
            openCreateCriteriaDialog.value -> CreateCriteriaDialog(
                actions = CreateCriteriaDialogActions(
                    dismissDialog = {
                        openCreateCriteriaDialog.value = false
                    },
                    confirmDialog = { text ->
                        coroutineScope.launch {
                            viewModel.saveCriteria(
                                uiState.decision!!.id, text
                            )
                        }
                        openCreateCriteriaDialog.value = false
                    }), decision = decision
            )
        }
    }
    uiState.decision?.let {
        when {
            openConfirmDeletionDialog.value -> ConfirmationDialog(
                actions = ConfirmationDialogActions(dismissDialog = {
                    openConfirmDeletionDialog.value = false
                },
                    confirmDialog = {
                        coroutineScope.launch {
                            viewModel.deleteDecision()
                            actions.navigateToHome()
                        }
                    })
            ) {
                Text(text = stringResource(id = R.string.deletion_confirmation))
            }
        }
    }
    Column(modifier = Modifier.padding(6.dp)) {
        uiState.decision?.let { decision ->
            ElevatedCard(
                elevation = CardDefaults.cardElevation(
                ), modifier = Modifier.fillMaxWidth()
            ) {
                Column(modifier = Modifier.padding(6.dp)) {
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = decision.title, style = MaterialTheme.typography.headlineLarge
                        )
                        IconButton(onClick = {
                            coroutineScope.launch {
                                openConfirmDeletionDialog.value = true
                            }
                        }) {
                            Icon(
                                imageVector = Icons.Filled.Delete,
                                contentDescription = "Delete Decision"
                            )
                        }
                    }
                    SpacerSmall()
                    HorizontalDivider()
                    SpacerSmall()
                    Text(text = decision.description)
                    SpacerLarge()
                    Row(
                        horizontalArrangement = Arrangement.End,
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        ElevatedButton(onClick = { actions.navigateToResults(decision.id) }) {
                            Text(text = "Results")
                        }
                    }
                }

            }

            ElevatedCard(
                elevation = CardDefaults.cardElevation(),
                modifier = Modifier.fillMaxWidth()
            ) {
                Column(modifier = Modifier.padding(6.dp)) {
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = stringResource(id = R.string.decision_overview_contestant_title),
                            style = MaterialTheme.typography.headlineMedium
                        )
                        Row {
                            IconButton(onClick = {
                                coroutineScope.launch {
                                    openCreateContestantDialog.value = true
                                }
                            }) {
                                Icon(
                                    imageVector = Icons.Filled.Add,
                                    contentDescription = "Add Contestant"
                                )
                            }
                        }

                    }
                    SpacerSmall()
                    HorizontalDivider()
                    SpacerSmall()
                    when (uiState.contestants.isEmpty()) {
                        true -> {
                            Row(
                                horizontalArrangement = Arrangement.Center,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                TextButton(onClick = {
                                    coroutineScope.launch {
                                        openCreateContestantDialog.value = true
                                    }
                                }) {
                                    Icon(
                                        imageVector = Icons.Filled.Add,
                                        contentDescription = "Add contestant"
                                    )
                                    Spacer(modifier = Modifier)
                                    Text(text = stringResource(id = R.string.decision_overview_no_contestant_button))
                                }
                            }
                        }

                        false -> {
                            DismissibleList(listItems = uiState.contestants.map {
                                {
                                    ListItem(headlineContent = { Text(text = it.name) },
                                        colors = ListItemDefaults.colors(
                                            containerColor = MaterialTheme.colorScheme.surfaceVariant,
                                        ),
                                        supportingContent = {
                                            it.information?.let {
                                                Text(
                                                    text = it,
                                                    style = MaterialTheme.typography.labelSmall
                                                )
                                            }
                                        },
                                        trailingContent = {
                                            IconButton(onClick = {
                                                actions.navigateToEvaluation(
                                                    decision.id,
                                                    it.id
                                                )
                                            }) {
                                                Icon(
                                                    imageVector = Icons.Default.Settings,
                                                    contentDescription = "Ratings"
                                                )
                                            }
                                        }
                                    )
                                }
                            }, actions = DismissibleListActions(deleteItem = { itemId ->
                                coroutineScope.launch {
                                    viewModel.removeContestant(uiState.contestants[itemId])
                                }
                                true
                            })
                            )
                        }
                    }
                }
            }
            ElevatedCard(
                elevation = CardDefaults.cardElevation(), modifier = Modifier.fillMaxWidth()
            ) {
                Column(modifier = Modifier.padding(6.dp)) {
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = stringResource(id = R.string.decision_overview_criteria_title),
                            style = MaterialTheme.typography.headlineMedium
                        )
                        Row {
                            IconButton(onClick = {
                                coroutineScope.launch {
                                    openCreateCriteriaDialog.value = true
                                }
                            }) {
                                Icon(
                                    imageVector = Icons.Filled.Add,
                                    contentDescription = "Add Criteria"
                                )
                            }
                            IconButton(onClick = {
                                coroutineScope.launch {
                                    actions.navigateToPrioritization(uiState.decision!!.id)
                                }
                            }, enabled = uiState.criteria.size > 1) {
                                Icon(
                                    imageVector = Icons.Filled.Settings,
                                    contentDescription = "Configure weights"
                                )
                            }
                        }

                    }
                    SpacerSmall()
                    HorizontalDivider()
                    SpacerSmall()
                    when (uiState.criteria.isEmpty()) {
                        true -> {
                            Row(
                                horizontalArrangement = Arrangement.Center,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                TextButton(onClick = {
                                    coroutineScope.launch {
                                        openCreateCriteriaDialog.value = true
                                    }
                                }) {
                                    Icon(
                                        imageVector = Icons.Filled.Add,
                                        contentDescription = "Add criteria"
                                    )
                                    Spacer(modifier = Modifier)
                                    Text(text = stringResource(id = R.string.decision_overview_no_criteria_button))
                                }
                            }
                        }

                        false -> {
                            DismissibleList(listItems = uiState.criteria.map {
                                {
                                    ListItem(
                                        headlineContent = { Text(text = it.text) },
                                        trailingContent = {
                                            Text(
                                                text = String.format(
                                                    format = "%.2f%%",
                                                    it.weight * 100
                                                )
                                            )
                                        },
                                        colors = ListItemDefaults.colors(
                                            containerColor = MaterialTheme.colorScheme.surfaceVariant,
                                        ),
                                    )
                                }
                            }, actions = DismissibleListActions(deleteItem = { itemId ->
                                coroutineScope.launch {
                                    viewModel.removeCriteria(uiState.criteria[itemId])
                                }
                                true
                            })
                            )
                        }
                    }
                }
            }
        }
    }
}