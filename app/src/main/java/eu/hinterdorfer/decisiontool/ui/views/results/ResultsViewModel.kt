package eu.hinterdorfer.decisiontool.ui.views.results

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.hinterdorfer.decisiontool.data.repository.DecisionRepository
import eu.hinterdorfer.decisiontool.data.repository.RatingRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ResultsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val decisionRepository: DecisionRepository,
    private val ratingRepository: RatingRepository
) : ViewModel() {

    private val _state = MutableStateFlow(ResultsState())

    val state: StateFlow<ResultsState> = _state.asStateFlow()

    private val decisionId: Long = checkNotNull(savedStateHandle["decisionId"])

    init {
        viewModelScope.launch {
            decisionRepository.findById(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        decision = result
                    )
                }
            }.launchIn(viewModelScope)
        }
        viewModelScope.launch {
            ratingRepository.queryResults(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        results = result
                    )
                }
            }.launchIn(viewModelScope)
        }
    }
}