package eu.hinterdorfer.decisiontool.ui.views.decision_overview

import eu.hinterdorfer.decisiontool.data.model.Contestant
import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.model.Decision


/**
 * UI State that represents
 **/
data class DecisionOverviewState(
    val decision: Decision? = null,
    val contestants: List<Contestant> = emptyList(),
    val criteria: List<Criteria> = emptyList()
)

/**
 * Home Actions emitted from the UI Layer
 * passed to the coordinator to handle
 **/
data class DecisionOverviewActions(
    val navigateToHome: () -> Unit = {},
    val navigateToPrioritization: (decisionId: Long) -> Unit = {},
    val navigateToEvaluation: (decisionId: Long, contestantId: Long) -> Unit = { _, _ -> },
    val navigateToResults: (decisionId: Long) -> Unit = { _ -> }
)