package eu.hinterdorfer.decisiontool.ui.components.sidenav

import eu.hinterdorfer.decisiontool.data.model.Decision


/**
 * UI State that represents HomeScreen
 **/
data class SidenavState(val decisions: List<Decision> = emptyList())

/**
 * Home Actions emitted from the UI Layer
 * passed to the coordinator to handle
 **/
data class SidenavActions(
    val navigateToNewDecision: () -> Unit = {},
    val navigateToDecision: (decisionId: Long) -> Unit = {},
    val navigateToHome: () -> Unit = {},
)