package eu.hinterdorfer.decisiontool.ui.views.contestant_evaluation

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.ListItem
import androidx.compose.material3.ListItemDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import eu.hinterdorfer.decisiontool.ui.components.labeled_content.LabeledContent
import eu.hinterdorfer.decisiontool.ui.theme.SpacerSmall
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

@Composable
fun ContestantEvaluationScreen(
    viewModel: ContestantEvaluationViewModel = hiltViewModel(),
    actions: ContestantEvaluationActions,
) {
    val coroutineScope = rememberCoroutineScope()
    val uiState by viewModel.state.collectAsState()

    Column(
        modifier = Modifier.padding(6.dp),
    ) {
        ElevatedCard(
            modifier = Modifier.fillMaxWidth()
        ) {
            LabeledContent(label = "Decision") {
                Text(
                    text = uiState.decision?.title ?: "?",
                    style = MaterialTheme.typography.headlineLarge
                )
            }
            LabeledContent(label = "Contestant") {
                Text(
                    text = uiState.contestant?.name ?: "?",
                    style = MaterialTheme.typography.headlineMedium
                )
            }
            SpacerSmall()
            HorizontalDivider()
            SpacerSmall()
            Text(text = "Please rate the following criteria relevant for the current decision")
        }
        LazyColumn(
            modifier = Modifier,
            contentPadding = PaddingValues(vertical = 8.dp) // 8.dp between each item
        ) {
            items(items = uiState.criteria.entries.toList()) { item ->
                ListItem(headlineContent = { Text(text = item.key.text) },
                    colors = ListItemDefaults.colors(
                        containerColor = MaterialTheme.colorScheme.surfaceVariant,
                    ),
                    supportingContent = {
                        Slider(
                            value = item.value,
                            onValueChange = { viewModel.updateCriteriaRating(item.key, it) },
                            colors = SliderDefaults.colors(
                                thumbColor = MaterialTheme.colorScheme.secondary,
                                activeTrackColor = MaterialTheme.colorScheme.secondary,
                                inactiveTrackColor = MaterialTheme.colorScheme.secondaryContainer,
                            ),
                            steps = 9,
                            valueRange = 0f..10f
                        )
                    }, trailingContent = {
                        Text(text = "${String.format("%2d", item.value.roundToInt())} / 10")
                    })
            }
        }
        SpacerSmall()
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            OutlinedButton(
                onClick = actions.cancel
            ) {
                Text("Cancel")
            }
            ElevatedButton(
                onClick = {
                    coroutineScope.launch {
                        viewModel.saveRating()
                    }
                    actions.afterSave()
                }
            ) {
                Text(text = "Save")
            }
        }

    }

}
