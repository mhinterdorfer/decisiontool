package eu.hinterdorfer.decisiontool.ui.navigation

import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument

abstract class NavigationRoute(
    val route: String,
    val arguments: List<NamedNavArgument> = emptyList(),
)

object HomeRoute : NavigationRoute(route = "home")

object DecisionOverviewRoute : NavigationRoute(
    route = "decision/{decisionId}",
    arguments = listOf(navArgument("decisionId") { type = NavType.LongType }),
)

object PrioritizeCriteriaRoute : NavigationRoute(
    route = "decision/{decisionId}/prioritize",
    arguments = listOf(navArgument("decisionId") { type = NavType.LongType }),
)

object ContestantEvaluationRoute : NavigationRoute(
    route = "decision/{decisionId}/evaluate/{contestantId}",
    arguments = listOf(
        navArgument("decisionId") { type = NavType.LongType },
        navArgument("contestantId") { type = NavType.LongType })
)

object ResultsRoute : NavigationRoute(
    route = "decision/{decisionId}/results",
    arguments = listOf(
        navArgument("decisionId") { type = NavType.LongType })
)
