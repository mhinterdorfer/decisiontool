package eu.hinterdorfer.decisiontool.ui.views.home

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.ListItemDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel

@Composable
fun HomeScreen(
    viewModel: HomeViewModel = hiltViewModel(),
    actions: HomeActions,
) {
    val uiState by viewModel.state.collectAsState()
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(6.dp),
        contentPadding = PaddingValues(vertical = 8.dp) // 8.dp between each item
    ) {
        items(items = uiState.decisions) { item ->
            ListItem(headlineContent = { Text(text = item.title) },
                colors = ListItemDefaults.colors(
                    containerColor = MaterialTheme.colorScheme.surfaceVariant,
                ),
                supportingContent = {
                    Text(text = item.description)
                },
                trailingContent = {
                    IconButton(onClick = {
                        actions.navigateToDecision(item.id)
                    }) {
                        Icon(
                            imageVector = Icons.Default.Settings,
                            contentDescription = "Decision details"
                        )
                    }
                }
            )

        }
    }
}
