package eu.hinterdorfer.decisiontool.ui.navigation

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import eu.hinterdorfer.decisiontool.ui.views.contestant_evaluation.ContestantEvaluationActions
import eu.hinterdorfer.decisiontool.ui.views.contestant_evaluation.ContestantEvaluationScreen
import eu.hinterdorfer.decisiontool.ui.views.criteria_prioritization.CriteriaPrioritizationActions
import eu.hinterdorfer.decisiontool.ui.views.criteria_prioritization.CriteriaPrioritizationScreen
import eu.hinterdorfer.decisiontool.ui.views.decision_overview.DecisionOverviewActions
import eu.hinterdorfer.decisiontool.ui.views.decision_overview.DecisionOverviewScreen
import eu.hinterdorfer.decisiontool.ui.views.home.HomeActions
import eu.hinterdorfer.decisiontool.ui.views.home.HomeScreen
import eu.hinterdorfer.decisiontool.ui.views.results.ResultsActions
import eu.hinterdorfer.decisiontool.ui.views.results.ResultsScreen

@Composable
fun MainNavHost(paddingValues: PaddingValues, navController: NavHostController) {
    Column(modifier = Modifier.padding(paddingValues)) {
        NavHost(
            navController = navController,
            startDestination = HomeRoute.route
        ) {
            composable(route = HomeRoute.route) {
                HomeScreen(actions = HomeActions(navigateToDecision = { decisionId ->
                    navController.navigate(
                        "decision/$decisionId"
                    )
                }))
            }
            composable(
                route = DecisionOverviewRoute.route,
                arguments = DecisionOverviewRoute.arguments
            ) {
                DecisionOverviewScreen(
                    actions = DecisionOverviewActions(
                        navigateToHome = {
                            navController.navigate(
                                HomeRoute.route
                            )
                        },
                        navigateToPrioritization = { decisionId -> navController.navigate(route = "decision/$decisionId/prioritize") },
                        navigateToEvaluation = { decisionId, contestantId ->
                            navController.navigate(route = "decision/$decisionId/evaluate/$contestantId")
                        },
                        navigateToResults = { decisionId -> navController.navigate("decision/$decisionId/results") })
                )
            }
            composable(
                route = PrioritizeCriteriaRoute.route,
                arguments = PrioritizeCriteriaRoute.arguments
            ) {
                CriteriaPrioritizationScreen(actions = CriteriaPrioritizationActions(cancel = {
                    navController.navigate(
                        HomeRoute.route
                    )
                }, afterSave = { navController.navigate(HomeRoute.route) }))
            }
            composable(
                route = ContestantEvaluationRoute.route,
                arguments = ContestantEvaluationRoute.arguments
            ) {
                ContestantEvaluationScreen(actions = ContestantEvaluationActions(cancel = {
                    navController.navigate(
                        HomeRoute.route
                    )
                }, afterSave = { navController.navigate(HomeRoute.route) }))
            }
            composable(
                route = ResultsRoute.route,
                arguments = ResultsRoute.arguments
            ) {
                ResultsScreen(actions = ResultsActions(navigateBack = {
                    navController.navigate(
                        HomeRoute.route
                    )
                }))
            }
        }
    }
}
