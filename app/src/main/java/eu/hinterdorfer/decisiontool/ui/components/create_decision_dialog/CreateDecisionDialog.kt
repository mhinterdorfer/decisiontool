package eu.hinterdorfer.decisiontool.ui.components.create_decision_dialog

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.paneTitle
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import eu.hinterdorfer.decisiontool.R
import eu.hinterdorfer.decisiontool.ui.theme.SpacerMedium
import eu.hinterdorfer.decisiontool.ui.theme.SpacerSmall

internal val DialogMinWidth = 280.dp
internal val DialogMaxWidth = 560.dp

@Composable
fun CreateDecisionDialog(
    actions: CreateDecisionDialogActions,
) {
    var name by remember {
        mutableStateOf("")
    }
    var description by remember {
        mutableStateOf("")
    }
    var nameValid by remember {
        mutableStateOf(false)
    }
    Dialog(
        onDismissRequest = actions.dismissDialog,
    ) {
        Box(
            modifier = Modifier
                .sizeIn(
                    minWidth = eu.hinterdorfer.decisiontool.ui.components.create_criteria_dialog.DialogMinWidth,
                    maxWidth = eu.hinterdorfer.decisiontool.ui.components.create_criteria_dialog.DialogMaxWidth
                )
                .then(Modifier.semantics { paneTitle = "Create decision dialog" }),
            propagateMinConstraints = true
        ) {
            ElevatedCard(
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 6.dp
                ),
            ) {
                Column(modifier = Modifier.padding(16.dp)) {
                    Text(
                        text = stringResource(id = R.string.create_decision_dialog_title),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.titleLarge
                    )
                }
                HorizontalDivider()
                Column(modifier = Modifier.padding(16.dp)) {
                    OutlinedTextField(
                        value = name,
                        onValueChange = {
                            name = it
                            nameValid = name.length >= 5
                        },
                        label = {
                            Text(
                                text = stringResource(
                                    id = R.string.create_decision_dialog_name_label
                                )
                            )
                        },
                        isError = !nameValid
                    )
                    OutlinedTextField(
                        value = description,
                        onValueChange = {
                            description = it
                        },
                        label = {
                            Text(
                                text = stringResource(
                                    id = R.string.create_decision_dialog_description_label
                                )
                            )
                        })
                    SpacerMedium()
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.End
                    ) {
                        ElevatedButton(onClick = actions.dismissDialog) {
                            Text(text = stringResource(id = R.string.create_decision_dialog_dismiss_text))
                        }
                        SpacerSmall()
                        FilledTonalButton(
                            onClick = {
                                actions.confirmDialog(
                                    name,
                                    description
                                )
                            },
                            enabled = nameValid
                        ) {
                            Text(text = stringResource(id = R.string.create_decision_dialog_confirm_text))
                        }
                    }
                }

            }
        }
    }
}



