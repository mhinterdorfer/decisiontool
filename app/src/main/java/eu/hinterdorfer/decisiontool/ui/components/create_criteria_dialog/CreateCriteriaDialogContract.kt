package eu.hinterdorfer.decisiontool.ui.components.create_criteria_dialog


/**
 * Home Actions emitted from the UI Layer
 * passed to the coordinator to handle
 **/
data class CreateCriteriaDialogActions(
    val dismissDialog: () -> Unit = {},
    val confirmDialog: (text: String) -> Unit = { text: String ->
        println(
            text
        )
    }
)