package eu.hinterdorfer.decisiontool.ui.views.contestant_evaluation

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.model.Rating
import eu.hinterdorfer.decisiontool.data.repository.ContestantRepository
import eu.hinterdorfer.decisiontool.data.repository.DecisionRepository
import eu.hinterdorfer.decisiontool.data.repository.RatingRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContestantEvaluationViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val decisionRepository: DecisionRepository,
    private val contestantRepository: ContestantRepository,
    private val ratingRepository: RatingRepository
) : ViewModel() {


    private val _state = MutableStateFlow(ContestantEvaluationState())

    val state: StateFlow<ContestantEvaluationState> = _state.asStateFlow()

    private val decisionId: Long = checkNotNull(savedStateHandle["decisionId"])
    private val contestantId: Long = checkNotNull(savedStateHandle["contestantId"])

    init {
        viewModelScope.launch {
            decisionRepository.findById(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        decision = result
                    )
                }
            }.launchIn(viewModelScope)
            decisionRepository.findCriteriasById(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        criteria = result.associateWith { 0.0f }.toMap(),
                    )
                }
            }.launchIn(viewModelScope)
            contestantRepository.findById(contestantId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        contestant = result
                    )
                }
            }.launchIn(viewModelScope)
        }
    }

    fun updateCriteriaRating(criteria: Criteria, newValue: Float) {
        _state.update {
            _state.value.copy(
                criteria = it.criteria.plus(criteria to newValue)
            )
        }

    }

    suspend fun saveRating() {
        val decisionId = _state.value.decision!!.id
        val contestantId = _state.value.contestant!!.id
        ratingRepository.deleteByContestant(contestantId)
        _state.value.criteria.forEach { entry ->
            ratingRepository.saveRating(
                Rating(
                    decisionId = decisionId,
                    contestantId = contestantId,
                    criteriaId = entry.key.id,
                    value = entry.value
                )
            )
        }
    }

}