package eu.hinterdorfer.decisiontool.ui.components.sidenav

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.hinterdorfer.decisiontool.data.repository.DecisionRepository
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SidenavViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    decisionRepository: DecisionRepository
) : ViewModel() {

    private val _state = mutableStateOf(SidenavState())

    val state: State<SidenavState> = _state

    init {
        viewModelScope.launch {
            decisionRepository.findAllDecisions().onEach { result ->
                _state.value = _state.value.copy(
                    decisions = result
                )
            }.launchIn(viewModelScope)
        }
    }

}