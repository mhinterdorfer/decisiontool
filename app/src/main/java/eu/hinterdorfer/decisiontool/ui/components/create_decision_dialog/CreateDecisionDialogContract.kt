package eu.hinterdorfer.decisiontool.ui.components.create_decision_dialog

data class CreateDecisionDialogState(val name: String = "", val description: String = "")


/**
 * Home Actions emitted from the UI Layer
 * passed to the coordinator to handle
 **/
data class CreateDecisionDialogActions(
    val dismissDialog: () -> Unit = {},
    val confirmDialog: (name: String, description: String) -> Unit = { name: String, description: String ->
        println(
            "$name, $description"
        )
    }
)