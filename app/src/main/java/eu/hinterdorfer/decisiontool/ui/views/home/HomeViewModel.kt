package eu.hinterdorfer.decisiontool.ui.views.home

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.hinterdorfer.decisiontool.data.repository.DecisionRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    decisionRepository: DecisionRepository
) : ViewModel() {

    private val _state = MutableStateFlow(HomeState())

    val state: StateFlow<HomeState> = _state.asStateFlow()

    init {
        viewModelScope.launch {
            decisionRepository.countOpenDecisions().onEach { result ->
                _state.value = _state.value.copy(
                    openDecisions = result
                )
            }.launchIn(viewModelScope)
            decisionRepository.findAllDecisions().onEach { result ->
                _state.value = _state.value.copy(
                    decisions = result
                )
            }.launchIn(viewModelScope)
        }
    }


}