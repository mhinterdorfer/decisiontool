package eu.hinterdorfer.decisiontool.ui.components.confirmation_dialog

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import eu.hinterdorfer.decisiontool.R
import eu.hinterdorfer.decisiontool.ui.components.create_contestant_dialog.ConfirmationDialogActions

@Composable
fun ConfirmationDialog(
    actions: ConfirmationDialogActions,
    content: @Composable () -> Unit
) {
    AlertDialog(
        icon = {
            Icon(
                painterResource(id = R.drawable.baseline_dangerous_24),
                contentDescription = "Dangerous Action"
            )
        },
        title = {
            Text(text = stringResource(id = R.string.confirm_dangerous_action_title))
        },
        text = {
            content()
        },
        onDismissRequest = actions.dismissDialog,
        confirmButton = {
            TextButton(
                onClick = actions.confirmDialog
            ) {
                Text("Confirm")
            }
        },
        dismissButton = {
            TextButton(
                onClick = actions.dismissDialog
            ) {
                Text("Dismiss")
            }
        })
}



