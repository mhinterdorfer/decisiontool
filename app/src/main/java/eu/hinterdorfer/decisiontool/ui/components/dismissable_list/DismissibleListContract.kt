package eu.hinterdorfer.decisiontool.ui.components.dismissable_list


data class DismissibleListActions(
    val deleteItem: (itemId: Int) -> Boolean = { false },
)
