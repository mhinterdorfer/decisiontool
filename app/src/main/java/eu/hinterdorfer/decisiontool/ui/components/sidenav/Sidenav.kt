package eu.hinterdorfer.decisiontool.ui.components.sidenav

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.List
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Home
import androidx.compose.material3.DrawerState
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import eu.hinterdorfer.decisiontool.R

@Composable
fun Sidenav(
    viewModel: SidenavViewModel = hiltViewModel(),
    actions: SidenavActions, drawerState: DrawerState, content: @Composable () -> Unit
) {
    val showSubMenu = remember {
        mutableStateOf(false)
    }

    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet {
                Text(
                    text = stringResource(id = R.string.app_name),
                    modifier = Modifier.padding(16.dp),
                    style = MaterialTheme.typography.titleLarge
                )
                HorizontalDivider()
                NavigationDrawerItem(
                    label = { Text(text = stringResource(id = R.string.home_title)) },
                    icon = {
                        Icon(
                            Icons.Filled.Home,
                            contentDescription = stringResource(id = R.string.home_description)
                        )
                    },
                    selected = false,
                    onClick = actions.navigateToHome
                )
                NavigationDrawerItem(
                    label = { Text(text = stringResource(id = R.string.new_decision_title)) },
                    icon = {
                        Icon(
                            Icons.Filled.Add,
                            contentDescription = stringResource(id = R.string.new_decision_description)
                        )
                    },
                    selected = false,
                    onClick = actions.navigateToNewDecision
                )
                NavigationDrawerItem(
                    label = { Text(text = stringResource(id = R.string.active_decisions_title)) },
                    icon = {
                        Icon(
                            Icons.AutoMirrored.Filled.List,
                            contentDescription = stringResource(id = R.string.active_decisions_title)
                        )
                    },
                    selected = false,
                    onClick = { showSubMenu.value = !showSubMenu.value }
                )
                AnimatedVisibility(visible = showSubMenu.value) {
                    DecisionMenu(
                        actions = actions
                    )
                }
                // other drawer items
            }
        },
        gesturesEnabled = true,
        content = content // set content of component to function parameter
    )
}

@Composable
fun DecisionMenu(
    viewModel: SidenavViewModel = hiltViewModel(),
    actions: SidenavActions
) {
    Column(Modifier.padding(start = 24.dp, end = 24.dp)) {
        viewModel.state.value.decisions.map { decision ->
            TextButton(onClick = { actions.navigateToDecision(decision.id) }) {
                Text(text = decision.title)
            }
        }
    }
}


