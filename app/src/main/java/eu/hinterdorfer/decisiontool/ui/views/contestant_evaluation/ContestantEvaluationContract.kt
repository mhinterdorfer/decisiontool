package eu.hinterdorfer.decisiontool.ui.views.contestant_evaluation

import eu.hinterdorfer.decisiontool.data.model.Contestant
import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.model.Decision

data class ContestantEvaluationState(
    val decision: Decision? = null,
    val contestant: Contestant? = null,
    val criteria: Map<Criteria, Float> = emptyMap(),
)

data class ContestantEvaluationActions(
    val cancel: () -> Unit = {},
    val afterSave: () -> Unit = {}
)
