package eu.hinterdorfer.decisiontool.ui.views.home

import eu.hinterdorfer.decisiontool.data.model.Decision


/**
 * UI State that represents HomeScreen
 **/
data class HomeState(val openDecisions: Int? = null, val decisions: List<Decision> = emptyList())

/**
 * Home Actions emitted from the UI Layer
 * passed to the coordinator to handle
 **/
data class HomeActions(
    val navigateToDecision: (decisionId: Long) -> Unit = {}
)