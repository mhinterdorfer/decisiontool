package eu.hinterdorfer.decisiontool.ui.views.decision_overview

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.hinterdorfer.decisiontool.data.model.Contestant
import eu.hinterdorfer.decisiontool.data.model.Criteria
import eu.hinterdorfer.decisiontool.data.repository.ContestantRepository
import eu.hinterdorfer.decisiontool.data.repository.CriteriaRepository
import eu.hinterdorfer.decisiontool.data.repository.DecisionRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DecisionOverviewViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val decisionRepository: DecisionRepository,
    private val contestantRepository: ContestantRepository,
    private val criteriaRepository: CriteriaRepository,
) : ViewModel() {

    private val _state = MutableStateFlow(DecisionOverviewState())

    val state: StateFlow<DecisionOverviewState> = _state.asStateFlow()

    private val decisionId: Long = checkNotNull(savedStateHandle["decisionId"])

    init {
        viewModelScope.launch {
            decisionRepository.findById(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        decision = result
                    )
                }
            }.launchIn(viewModelScope)
            decisionRepository.findContestantsById(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        contestants = result
                    )
                }
            }.launchIn(viewModelScope)
            decisionRepository.findCriteriasById(decisionId).onEach { result ->
                _state.update {
                    _state.value.copy(
                        criteria = result
                    )
                }
            }.launchIn(viewModelScope)
        }
    }

    suspend fun saveContestant(decisionId: Long, name: String, information: String) {
        contestantRepository.insertAllContestants(
            Contestant(
                decisionId = decisionId,
                name = name,
                information = information
            )
        )
    }

    suspend fun saveCriteria(decisionId: Long, text: String) {
        criteriaRepository.insertAllCriterias(Criteria(decisionId = decisionId, text = text))
    }

    suspend fun deleteDecision() {
        decisionRepository.deleteDecision(_state.value.decision!!)
    }

    suspend fun removeContestant(contestant: Contestant) {
        contestantRepository.deleteContestant(contestant)
    }

    suspend fun removeCriteria(criteria: Criteria) {
        criteriaRepository.deleteCriteria(criteria)
    }
}