package eu.hinterdorfer.decisiontool.ui.components.criteria_prioritization_prompt

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import eu.hinterdorfer.decisiontool.data.model.Criteria

@Composable
fun CriteriaPrioritizationPrompt(
    criteriaA: Criteria,
    criteriaB: Criteria,
    onChooseCriteria: (criteria: Criteria) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.SpaceAround,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        ElevatedButton(
            onClick = { onChooseCriteria(criteriaA) }
        ) {
            Text(text = criteriaA.text)
        }
        Text(text = "Choose the more important Criteria")
        ElevatedButton(
            onClick = { onChooseCriteria(criteriaB) }
        ) {
            Text(text = criteriaB.text)
        }
    }
}

@Preview
@Composable
fun CriteriaPrioritizationPromptPreview() {
    Surface(modifier = Modifier.fillMaxSize()) {
        CriteriaPrioritizationPrompt(
            criteriaA = Criteria(decisionId = 0, text = "testA"),
            criteriaB = Criteria(decisionId = 0, text = "testB"),
            onChooseCriteria = { criteria: Criteria -> println(criteria.text) }
        )
    }

}


