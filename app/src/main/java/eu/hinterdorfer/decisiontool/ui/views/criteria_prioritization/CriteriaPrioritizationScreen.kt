package eu.hinterdorfer.decisiontool.ui.views.criteria_prioritization

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.ListItem
import androidx.compose.material3.ListItemDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import eu.hinterdorfer.decisiontool.R
import eu.hinterdorfer.decisiontool.ui.components.criteria_prioritization_prompt.CriteriaPrioritizationPrompt
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

@Composable
fun CriteriaPrioritizationScreen(
    viewModel: CriteriaPrioritizationViewModel = hiltViewModel(),
    actions: CriteriaPrioritizationActions,
) {
    val coroutineScope = rememberCoroutineScope()
    var currentCombination by remember {
        mutableIntStateOf(0)
    }

    val uiState by viewModel.state.collectAsState()

    if (currentCombination < uiState.combinations.size) {
        CriteriaPrioritizationPrompt(
            criteriaA = uiState.combinations[currentCombination].first,
            criteriaB = uiState.combinations[currentCombination].second
        ) {
            viewModel.prioritizeCriteria(it)
            currentCombination++
        }
    } else {
        ResultOverview(actions = actions, coroutineScope = coroutineScope)
    }
}

@Composable
fun ResultOverview(
    viewModel: CriteriaPrioritizationViewModel = hiltViewModel(),
    actions: CriteriaPrioritizationActions,
    coroutineScope: CoroutineScope
) {
    val uiState by viewModel.state.collectAsState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(6.dp),
    ) {
        Text(
            text = stringResource(id = R.string.prioritization_results),
            style = MaterialTheme.typography.headlineLarge
        )
        HorizontalDivider()
        LazyColumn(
            modifier = Modifier,
            contentPadding = PaddingValues(vertical = 8.dp) // 8.dp between each item
        ) {
            items(items = uiState.criteria.entries.toList()) { entry ->
                ListItem(headlineContent = { Text(text = entry.key.text) },
                    colors = ListItemDefaults.colors(
                        containerColor = MaterialTheme.colorScheme.surfaceVariant,
                    ),
                    supportingContent = {
                        SkillBar(
                            value = entry.value,
                            maxValue = uiState.combinations.size.toFloat()
                        )
                    })
            }
        }
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            OutlinedButton(
                onClick = actions.cancel
            ) {
                Text(text = "Cancel")
            }
            ElevatedButton(onClick = {
                coroutineScope.launch {
                    viewModel.saveWeights()
                }
                actions.afterSave()
            }) {
                Text(text = "Save Weights")
            }
        }
    }
}

@Composable
fun SkillBar(maxValue: Float = 100.0f, value: Float) {
    val gradient = Brush.linearGradient(
        listOf(
            Color(0xFFF95075),
            Color(0xFFBE6BE5)
        )
    )

    val progressFactor by remember {
        mutableFloatStateOf(value / maxValue)
    }

    Row(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            .height(32.dp)
            .border(
                width = 2.dp,
                color = MaterialTheme.colorScheme.onBackground,
                shape = RoundedCornerShape(32.dp)
            )
            .clip(
                RoundedCornerShape(
                    topStartPercent = 50,
                    topEndPercent = 50,
                    bottomEndPercent = 50,
                    bottomStartPercent = 50
                )
            )
            .background(Color.Transparent),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Button(
            contentPadding = PaddingValues(1.dp),
            onClick = { },
            modifier = Modifier
                .fillMaxWidth(progressFactor)
                .fillMaxHeight()
                .background(brush = gradient),
            enabled = false,
            elevation = null,
        ) {

            Text(
                text = "${(progressFactor * 10000).roundToInt() / 100.0f}%",
                modifier = Modifier
                    .fillMaxWidth(),
                color = MaterialTheme.colorScheme.onPrimary,
                textAlign = TextAlign.Center
            )
        }
    }
}