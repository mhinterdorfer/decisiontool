package eu.hinterdorfer.decisiontool

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import eu.hinterdorfer.decisiontool.data.AppDatabase
import eu.hinterdorfer.decisiontool.data.dao.ContestantDAO
import eu.hinterdorfer.decisiontool.data.dao.CriteriaDAO
import eu.hinterdorfer.decisiontool.data.dao.DecisionDAO
import eu.hinterdorfer.decisiontool.data.dao.RatingDAO
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, "decision-tool")
            .build()

    @Provides
    fun provideDecisionDao(appDatabase: AppDatabase): DecisionDAO =
        appDatabase.decisionDAO()

    @Provides
    fun provideContestantDao(appDatabase: AppDatabase): ContestantDAO =
        appDatabase.contestantDAO()

    @Provides
    fun provideCriteriaDao(appDatabase: AppDatabase): CriteriaDAO = appDatabase.criteriaDAO()

    @Provides
    fun provideRatingDao(appDatabase: AppDatabase): RatingDAO = appDatabase.ratingDAO()
}